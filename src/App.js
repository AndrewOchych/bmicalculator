import React, { useState } from "react"
import Slider from './components/Slider'

function calculateBMI(height, weight) {
    return weight / ((height / 100) ** 2)
}

function tellBMI(bmi) {
    return bmi <= 18.5 ? "Недостатня маса" :
        bmi <= 24.9 ? "Норма" :
            bmi <= 29.9 ? "Надлишкова маса" :
                "Ожиріння "
}

function tellBMIColor(bmi) {
    return bmi <= 18.5 ? "warning" :
        bmi <= 24.9 ? "success" :
            bmi <= 29.9 ? "warning" :
                "danger"
}

export default function App() {
    let [height, setHeight] = useState(175)
    let [weight, setWeight] = useState(75)
    let bmi = calculateBMI(height, weight)
    let bmiText = tellBMI(bmi)
    let bmiColor = tellBMIColor(bmi)
    return <div className="container p-3">
        <h1 className="h3 mb-3" style={{ textAlign: 'center' }}>BMI Calculator</h1>
        <Slider min={140} max={200} value={height} title="Ріст, см" onChange={setHeight} />
        <Slider min={30} max={180} value={weight} title="Вага, кг" onChange={setWeight} />
        <hr style={{ maxWidth: '500px', margin: '0 auto' }} />
        <div className="mt-2" style={{ textAlign: 'center', paddingTop: '10px' }}>
            BMI: <strong>{bmi.toFixed(1)}</strong>
            {" "}
            <span className={`badge badge-${bmiColor}`}>{bmiText}</span>
        </div>
    </div>
}