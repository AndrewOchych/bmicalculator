import React from "react"
import PT from "prop-types"

export default function Slider({ min, max, value, title, onChange }) {
  return <div style={{ maxWidth: '500px', margin: '0 auto', marginBottom: '10px' }}>
    <label>
      <strong>{title}</strong><br />
      <input type="range" min={min} max={max} value={value}
        onChange={e => onChange(parseInt(e.target.value))} style={{ width: "25rem" }} />
      {" "}
      {value}
    </label>
  </div >
}
Slider.propTypes = {
  min: PT.number.isRequired,
  max: PT.number.isRequired,
  value: PT.number.isRequired,
  title: PT.string.isRequired,
  onChange: PT.func.isRequired,
}